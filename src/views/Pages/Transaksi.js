import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { PUBLIC_URL } from "../../constants";

class Transaksi extends Component {

  constructor(props) {
    super(props);

    this.state = {
      dropdown: false
    };
  }

  render() {

    return (
      <div>
        <div className="card kartu" style={{ marginTop: 70 }}>
          <header className="card-header" style={{ height: 300, marginTop: -100 }}>
            <figure className="image " style={{ marginLeft: 150 }}>
              <img src={PUBLIC_URL + '/img/p5.png'} />
            </figure>
          </header>
          <div className="card-content">
            <div className="media">
              <div className="media-content">

                <h1>Payment</h1>

                <p>Make a payment before</p>
                <hr />
                <p>Friday, 30 Nov 23.59</p>
                <hr />
                <p>Please transfer to</p>
                <hr />
                <p>Account Number</p>
                <p>099223455151</p>
                <p>Account Holder Name</p>
                <p>PT.Nusantara</p>
                <hr />
                <p>Total Payment</p>
                <p>Rp. 1.200.000</p>
                <div className="content">
                  <div class="file has-name is-boxed">
                    <label class="file-label">
                      <input class="file-input" type="file" name="resume" />
                      <span class="file-cta">
                        <span class="file-icon">
                          <i class="fa fa-upload" />
                        </span>
                        <span class="file-label">
                          Upload Pembayaran
                              </span>
                      </span>
                      <span class="file-name">
                        file bisa berupa jpg atau png
                            </span>
                    </label>
                  </div>
                  <br />
                  <NavLink to="/StatusBooking"><a className="button is-success">My Booking</a></NavLink>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}
export default Transaksi;