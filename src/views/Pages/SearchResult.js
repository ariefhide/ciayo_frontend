import React, { Component } from 'react';
import { PUBLIC_URL } from "../../constants";
import { NavLink } from 'react-router-dom';

class SearchResult extends Component {

  constructor(props) {
    super(props);

    this.state = {
      dropdown: false
    };
  }

  render() {

    return (
      <div>
        <div className="card kartu">
          <section class="hero is-info" style={{marginTop:50}}>
            <div class="hero-body">
              <div class="container">
                <h1 class="title">
                  Silahkan pilih bus yang tersedia
                </h1>
              </div>
            </div>
          </section>
          <header class="card-header" style={{ height: 300,marginTop:-100 }}>
            <figure className="image " style={{ marginLeft: 150 }}>
              <img src={PUBLIC_URL + '/img/p2.png'} />
            </figure>
          </header>

          <div className="columns" style={{ marginLeft:30}}>

            <div className="column is-3">
              <div className="card-image">
                <NavLink to="/BookingSummary">
                  <figure className="image is-3by2 ">
                    <img src={PUBLIC_URL + '/img/busMelody.jpg'} />
                  </figure>
                </NavLink>
              </div>

              <div className="card-content">
                <div className="media">
                  <div className="media-content">
                    <p className="title is-4">Melody</p>
                    <p className="subtitle is-6">
                      <span className="icon is-small is-left">
                        <i className="fa fa-bus" />
                      </span>
                      47 seat
                      </p>
                  </div>
                </div>

                <div className="content">
                  <p>
                    <span className="icon is-small is-left">
                      <i className="fa fa-tag" />
                    </span>
                    Rp 1.200.00,00 / 3 Day
                    </p>
                  <br />
                </div>
              </div>
            </div>

            <div className="column is-3">
              <div className="card-image ">
                <NavLink to="/BookingSummary">
                  <figure className="image is-3by2 ">
                    <img src={PUBLIC_URL + '/img/busMelody.jpg'} />
                  </figure>
                </NavLink>
              </div>

              <div className="card-content">
                <div className="media">
                  <div className="media-content">
                    <p className="title is-4">Melody</p>
                    <p className="subtitle is-6">
                      <span className="icon is-small is-left">
                        <i className="fa fa-bus" />
                      </span>
                      47 seat
                    </p>
                  </div>
                </div>

                <div className="content">
                  <p>
                    <span className="icon is-small is-left">
                      <i className="fa fa-tag" />
                    </span>
                    Rp 1.200.00,00 / 3 Day
                  </p>
                  <br />
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SearchResult;  