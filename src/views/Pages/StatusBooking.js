import React, { Component } from 'react';
import MyBooking from './MyBooking';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import BookingComplete from './BookingComplete';
import BookingOngoing from './BookingOngoing';
import Transaksi from './Transaksi';
import DetailBookingComplete from './DetailBookingComplete';

class StatusBooking extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  render() {
    return (
			<BrowserRouter>
      <div>
			<MyBooking/>

				<Switch>
					<Route path="/BookingOngoing" component={BookingOngoing} />
					<Route path="/BookingComplete" component={BookingComplete} />
          <Route path="/Transaksi" component={Transaksi}/>
          <Route path="/DetailBookingComplete" component={DetailBookingComplete}/>
				</Switch>
        
    	</div>
			</BrowserRouter>
          );
        }
      }
      
export default StatusBooking;