import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { PUBLIC_URL } from "../../constants";

class BookingDetail extends Component {

  constructor(props) {
    super(props);

    this.state = {
      dropdown: false
    };
  }

  render() {

    return (
      <div>
        <div className="card kartu" style={{marginTop:70}}>
          <header className="card-header" style={{ height: 300, marginTop: -100 }}>
            <figure className="image " style={{ marginLeft: 150 }}>
              <img src={PUBLIC_URL + '/img/p4.png'} />
            </figure>
          </header>
          <div className="card-content">
            <div className="media">
              <div className="media-content">
                
                <h2>Booking Detail</h2>

                <form>
                  <div class="field">
                    <div class="control">
                      Name
                        <input class="input is-primary" type="text" placeholder="Masukan nama" />
                    </div>
                    <div class="control">
                      Phone
                        <input class="input is-primary" type="number" placeholder="Masukan nomor telpon" />
                    </div>
                    <div class="control">
                      Departure Location
                        <input class="input is-primary" type="text" placeholder="Departure Location" />
                    </div>
                    <div class="control">
                      Departure Time
                        <input class="input is-primary" type="text" placeholder="Departure Time" />
                    </div>
                    <br />

                    <div className="content">
                      <NavLink to="/Transaksi"><a className="button is-success">BOOKING</a></NavLink>
                    </div>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}
export default BookingDetail;