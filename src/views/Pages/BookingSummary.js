import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { PUBLIC_URL } from "../../constants";

class BookingSummary extends Component {

  constructor(props) {
    super(props);

    this.state = {
    };
  }

  render() {

    return (

      <div>
        <div className="card kartu" style={{marginTop:70}}>
          <header className="card-header" style={{ height: 300, marginTop: -100 }}>
            <figure className="image " style={{ marginLeft: 150 }}>
              <img src={PUBLIC_URL + '/img/p3.png'} />
            </figure>
          </header>
          <div className="card-content">
            <div className="media">
              <div className="media-content">

                <table className="table is-hoverable">
                  <thead>
                    <tr>
                      <th><abbr title="Position">Departure City</abbr></th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Bus Type</th>
                      <th>Facility</th>
                      <th>Price</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th>
                          <span className="icon is-small is-left">
                            <i className="fa fa-bus" />
                          </span>
                          Jakarta
                      </th>
                      <td>Wed, 26 Nov</td>
                      <td>Friday, 28 Nov</td>
                      <td>Melody Jetbus 2 SHD 47 seat</td>
                      <td> TV Toilet Wifi Full AC</td>
                      <td>
                          <span className="icon is-small is-left">
                          <i className="fa fa-tag" />
                          </span>Rp 1.200.00,00 / 3 Day
                      </td>
                    </tr>
                   </tbody> 
                  </table>

                
              </div>
            </div>

            <div className="content">
              <NavLink to="/BookingDetail"><a className="button is-success">SELECT</a></NavLink>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default BookingSummary;  