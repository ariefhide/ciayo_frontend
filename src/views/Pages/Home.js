import React, { Component } from 'react';
import {PUBLIC_URL} from "../../constants";
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Axios from 'axios';

import { connect } from 'react-redux';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      persons: [],
      kota: "",
    };
  }

  componentDidMount() {

   /* Axios.get(`http://localhost:5000/users`)
      .then(res => {
        console.log(res);
        this.setState({ persons: res.data });
      });
    */
  }

 
  render() {
    return (
     
      <div>
      
      <Carousel className="header" showThumbs={false} autoPlay interval={3000} infiniteLoop class="carousel" style={{hight:350}}>
				<div>
					<img src={PUBLIC_URL +'/img/bus.jpg'} />
				</div>
				<div>
					<img src={PUBLIC_URL +'/img/bus2.jpg'} />
				</div>
				<div>
					<img src={PUBLIC_URL +'/img/bus3.jpg'} />
				</div>
			</Carousel>
      

        
        <div className="columns">
          <div className="column is-10 is-offset-2">
            <h1 className="judul">Bus Reservation</h1>
          </div>
        </div>
        <div className="columns">
          <div className="column is-8 is-offset-2">
            <h1 style={{ color: 'white', fontSize: '2.2vw' }}>Selamat datang di Transbus</h1>
            <p style={{ color: 'white', fontSize: '1.2vw' }}>Mau pergi jalan-jalan, travelling atau kemana aja pake bus? booking pake transbus aja!</p>
            
          </div>
        </div>       

                     
                    {/*<ul>
                      {this.state.persons.map(person =>
                        <li key={person.userID}>{person.userEmail} + {person.userID}</li>
                      )}
                      </ul>
                    */}  
                     

      </div>


    );
  }
}

const mapStateToProps = (state) => {
  return state
}

/*const mapDispachToProps = (dispach) => {
    return{
      changeColor: () => dispach ({ type:'CHANGE_COLOR'}),
      onDel: () => dispach ({ type:'DEL'})
    }
}*/

export default connect(mapStateToProps, /*mapDispachToProps*/)(Home);