import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class DetailBookingComplete extends Component {

  constructor(props) {
    super(props);

    this.state = {
      dropdown: false
    };
  }

  render() {

    return (
      <div>
        <div className="card kartu">
          <div className="card-content">
            <div className="media">
              <div className="media-content">
                <p className="subtitle is-6">
                  <span className="icon is-small is-left">
                    <i className="fa fa-bus" />
                  </span>
                  Jakarta
                    </p>
                <p>Wed, 26 Nov -Friday, 28 Nov</p>

                <form>
                  
                  <div className="field">
                    <div className="control">
                      Name
                        <input className="input " type="text" placeholder="Masukan nama" disabled/>
                    </div>
                    <div className="control">
                      Phone
                        <input className="input " type="number" placeholder="Masukan nomor telpon" disabled/>
                    </div>
                    <div className="control">
                      Departure Location
                        <input className="input " type="text" placeholder="Departure Location" disabled/>
                    </div>
                    <div className="control">
                      Departure Time
                        <input className="input " type="text" placeholder="Departure Time" disabled/>
                    </div>
                    <br />

                    <div className="content">
                      <NavLink to="/MyBooking"><a className="button is-success">Kembali</a></NavLink>
                    </div>
                  </div>
                  
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}
export default DetailBookingComplete;