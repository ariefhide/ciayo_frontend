import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class BookingOngoing extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  render() {
    return (
      <div>
        <div class="card kartu">
          <header class="card-header">
            <p class="card-header-title">
              Booking ID
            </p>
          </header>
          <div class="card-content">
            <div class="content">
              <br/>
                <time datetime="2018-11-26">Jakarta, Wed 26 Nov 2018</time>
            </div>
          </div>
          <NavLink to="/Transaksi">
            <footer class="card-footer">
                <a class="card-footer-item">Pay</a>
            </footer>
            </NavLink>  
          </div>

        </div>
          );
        }
      }
      
export default BookingOngoing;