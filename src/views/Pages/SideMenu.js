import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Date from './Date';
import Axios from 'axios';



function searchingFor(kota) {
  return function (x) {
    return x.userName.toLowerCase().includes(kota.toLowerCase()) || false;
  }
}

class SideMenu extends Component {

  constructor(props) {
    super(props);

    this.state = {
      dropdown: false,
      persons: [],
      kota: "",
      dropKota: false,
    };
  }
  dropdownProfile = () => {
    const { dropdown } = this.state
    if (dropdown === false) {
      this.setState({ dropdown: true });
    } else {
      this.setState({ dropdown: false });
    }
  }
  toggle = (item) => {
    this.setState({ [item]: !this.state[item] });
  }



  componentDidMount() {

    Axios.get(`http://localhost:5000/users`)
      .then(res => {
        console.log(res);
        this.setState({ persons: res.data });
      });
  }

  handleSearch = (e) => {
    const { dropKota } = this.state
    if (dropKota === false) {
      this.setState({kota: e.target.value, dropKota: true  });
    } else {
      this.setState({kota: e.target.value, dropKota: false });
    }
  }
 
  render() {

    const { dropdown, kota, persons, dropKota } = this.state
    return (
      <div style={{boxShadow: '3px 3px 10px grey',marginTop:70,width:'100%',height:'auto',background:'#209cee'}}>

        <aside className="menu" style={{ paddingTop:30,paddingLeft:10 }} >
        
          <p className="menu-label" style={{fontSize:20,color:'white'}}>
            SEARCH BUS
              </p>
          <ul className="menu-list">
            <li>
              <Date />
            </li>
            <li>
              <p style={{color:'white'}}>Bus type</p>
              <div className={`dropdown ${dropdown && 'is-active'} is-left`}>
                <div className="dropdown-trigger" >
                  <button
                    onClick={this.dropdownProfile}
                    className="button"
                    aria-haspopup="true"
                    aria-controls="dropdown-menu6"
                  >
                    <span >Bus Type</span>
                    <span className="icon is-small">
                      <i className="fa fa-angle-down" aria-hidden="true"></i>
                    </span>
                  </button>
                </div>
                <div className="dropdown-menu" id="dropdown-menu6" role="menu">
                  <div className="dropdown-content">
                    <a className="dropdown-item" >
                      Jet bus 2 hd
                        </a>
                    <a className="dropdown-item">
                      Jet bus 3 hd
                        </a>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <p style={{color:'white'}}>Departure City</p>
              <input type="text" onChange={(e) => this.handleSearch(e)} value={kota} />
              <div className={`dropdown ${dropKota && 'is-active'}`}>
                <div className="dropdown-trigger">
                </div>
                <div className="dropdown-menu" id="dropdown-menu" role="menu">
                  {
                    persons.filter(searchingFor(kota)).map(person => (
                      <div key={person.userID} className="dropdown-content">
                        <a>{person.userName}</a>
                      </div>
                    ))}
                </div>

              </div>
            </li>
            
            <br />
            
            <li>
            <Link to="/SearchResult"><div className="button is-success" style={{ width: 100}}>SEARCH</div></Link>
            </li>
          </ul>
        </aside>
        
      </div>
    );
  }
}

export default SideMenu;  