import React, { Component } from 'react';
import { BrowserRouter, Route, NavLink, Switch } from 'react-router-dom';
import { PUBLIC_URL } from "../../constants";


class MyBooking extends Component {

  constructor(props) {
    super(props);

    this.state = {
      purchases: "is-active",
      completed: "",
    };
  }
  handlePurchases = () => {
    const { purchases, completed } = this.state
    if (purchases == "" || completed == "is-active") {
      this.setState({ purchases: "is-active", completed: "" })
    }
  }
  handleCompleted = () => {
    const { purchases, completed } = this.state
    if (completed == "" || purchases == "is-active") {
      this.setState({ purchases: "", completed: "is-active" })
    }
  }

  render() {
    const { purchases, completed } = this.state
    return (
      <div>
          <header className="card-header" style={{ height: 300, marginTop: -100 }}>
            <figure className="image " style={{ marginLeft: 150 }}>
              <img src={PUBLIC_URL + '/img/p6.png'} />
            </figure>
          </header>

        <div className="card kartu" >
          
          


          <div className="card-content">
            <div className="media">
              <div className="media-content">

                <div className="tabs is-centered is-boxed is-medium">
                  <ul>
                    <li className={purchases}>
                      <NavLink to="/BookingOngoing">
                        <a onClick={this.handlePurchases}>
                          <span>Ongoing Purchases</span>
                        </a>
                      </NavLink>
                    </li>
                    <li className={completed}>
                      <NavLink to="/BookingComplete">
                        <a onClick={this.handleCompleted}>
                          <span>Completed</span>
                        </a>
                      </NavLink>
                    </li>
                  </ul>
                </div>

              </div>
            </div>
          </div>
        </div>

      </div>


    );
  }
}
export default MyBooking;