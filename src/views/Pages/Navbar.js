import React, { Component } from 'react';
import 'bulma/css/bulma.css';
import { NavLink } from 'react-router-dom';
import { PUBLIC_URL } from "../../constants";
import axios from 'axios';

import { connect } from 'react-redux';

/*function searchingFor(user) {
  return function (x) {
    return x.userEmail.toLowerCase().includes(user.toLowerCase()) || false;
  }
}*/

import { setUser } from '../../redux/actions/AppAction';

class Navbar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      navbar: "navbar-menu",
      dropdown: "dropdown is-right",
      icon: "fa fa-eye-slash",
      password: "password",
      modalEdit: false,
      modalLogin: false,
      modalRegis: false,
      login: false,
      data:{},
      error:{},
      loading:false,

      apiUserID: "",
      apiUserName: "",
      apiUserEmail: "",
      apiUserPassword: "",
      apiUserPhone: "",

      emailLogin: "",
      passwordLogin: "",
      namaRegist: "",
      emailRegist: "",
      passwordRegist: "",
      phoneRegist: "",
      namaRegistError: "*nama tidak boleh kosong",
      passwordRegistError: "",
      repos: null,
      persons: [],
    };
  }

  handleNavbar = () => {
    if (this.state.navbar === "navbar-menu") {
      this.setState({ navbar: "navbar-menu is-active" })
    } else {
      this.setState({ navbar: "navbar-menu" })
    }
  }
  dropdownProfile = () => {
    if (this.state.dropdown === "dropdown is-right") {
      this.setState({ dropdown: "dropdown is-right is-active" })
    } else {
      this.setState({ dropdown: "dropdown is-right" })
    }
  }
  handleEdit = (e) => {
    this.setState({ modalEdit: true });
  }
  toggle = (item) => {
    this.setState({ [item]: !this.state[item] });
  }
  handlePassword = () => {
    if (this.state.password === "password") {
      this.setState({ password: "text", icon: "fa fa-eye" })
    } else if (this.state.password === "text") {
      this.setState({ password: "password", icon: "fa fa-eye-slash" })
    }
  }
  handleLogin = (e) => {

    this.setState({ modalLogin: true });

  }
  handleRegister = (e) => {
    this.setState({ modalRegis: true });
  }
  handleAkun = (item) => {

    this.setState({ [item]: !this.state[item] });

  }
  logout = () => {
    this.setState({ login: false, emailLogin: "", passwordLogin: "", })
    alert("Terima kasih")
  }


  handleSubmitLogin = (e) => {

    e.preventDefault();

    

    const data = {
      userEmail: e.target.elements.emailLogin.value,
      userPass: e.target.elements.passwordLogin.value,
    }

    //localStorage.setItem('data', JSON.stringify(data));

    //axios.get(`http://localhost:5000/users/${user}`)
    //.then((res) => {
    //console.log(res);
    //})
    if (data) {
      axios.post(`http://localhost:5000/login`, data)
        .then((res) => {
          console.log(res, "ress login")
          if (res.data.code === 204) {
            alert("email atau password salah")
            this.setState({ modalLogin: true })
          } else {
            alert("login berhasil")
            this.props.setUser(res.data.user)
            this.setState({
              login: true,
              loading:true,
              /*
              apiUserID: res.data.user.userID,
              apiUserEmail: res.data.user.userEmail,
              apiUserName: res.data.user.userName,
              apiUserPassword: res.data.user.userPass,
              apiUserPhone: res.data.user.userPhone
              */
            })
          }
        })
    } else return;
  }


  /*componentWillMount(){
    const data = JSON.parse(localStorage.getItem('data'));
    console.log(data)
  }*/

  componentWillMount() {
	}

  componentDidMount() {



    axios.get(`http://localhost:5000/users/`)
      .then(res => {
        console.log(res, 'res get data');
        this.setState({ persons: res.data });
      });
  }




  handleEmailLogin = (e) => {
    e.preventDefault()
    this.setState({ emailLogin: e.target.value });
  }
  handlePasswordLogin = (e) => {
    e.preventDefault()
    this.setState({ passwordLogin: e.target.value });
  }


  handleSubmitRegist = (e) => {
    e.preventDefault();

    
    const data = {
      userName: e.target.elements.namaRegist.value,
      userEmail: e.target.elements.emailRegist.value,
      userPass: e.target.elements.passwordRegist.value,
      userPhone: e.target.elements.phoneRegist.value,
    }
    //axios.get(`http://localhost:5000/users/${user}`)
    //.then((res) => {
    //console.log(res);
    //})
    if (data) {
      axios.post(`http://localhost:5000/users`, data)
        .then((res) => {
          console.log(res, "ress regist")
          if (res.data.message === "success") {
            alert("Registrasi berhasil silahkan Login")
            this.setState({ modalRegis: false })
          } else {
            alert("Email sudah ada")
          }
        })
    } else return;
  }



  /*
  if (this.state.passwordRegist.length < 6) {
    this.setState({ passwordRegistError: "password kurang kuat" })
  } else {
    this.setState({ passwordRegistError: "" })
  }
  */
  handleNamaRegist = (e) => {
    this.setState({ namaRegist: e.target.value })
  }
  handlePasswordRegist = (e) => {
    this.setState({ passwordRegist: e.target.value })
  }
  handleEmailRegist = (e) => {
    this.setState({ emailRegist: e.target.value })
  }
  handlePhoneRegist = (e) => {
    this.setState({ phoneRegist: e.target.value })
  }

  handleSubmitEdit = (e) => {
    e.preventDefault();

    const data = {
      userName: e.target.elements.namaEdit.value,
      userEmail: e.target.elements.emailEdit.value,
      userPass: e.target.elements.passwordEdit.value,
      userPhone: e.target.elements.phoneEdit.value,
      userID: this.state.apiUserID,
    }



    if (data) {
      axios.put(`http://localhost:5000/users/${data.userID}`, data)
        .then((res) => {
          console.log(res, "ress")
          if (res.data.message === "success") {
            alert("Profil berhasil di edit, silahkan login kembali")
            this.setState({ modalEdit: false, login: false })
          } else {
            alert("Email sudah ada")
          }
        })
    } else return;
  }

  handleNamaEdit = (e) => {
    this.setState({ apiUserName: e.target.value })
  }
  handlePasswordEdit = (e) => {
    this.setState({ apiUserPass: e.target.value })
  }
  handleEmailEdit = (e) => {
    this.setState({ apiUserEmail: e.target.value })
  }
  handlePhoneEdit = (e) => {
    this.setState({ apiUserPhone: e.target.value })
  }




  render() {
    const { user } = this.props
    const { dropdown,
      modalEdit,
      modalLogin,
      modalRegis,
      password,
      icon,
      emailLogin,
      passwordLogin,
      login,
      namaRegist,
      emailRegist,
      passwordRegist,
      namaRegistError,
      passwordRegistError,
      phoneRegist,
      apiUserName,
      apiUserEmail,
      apiUserPassword,
      apiUserPhone,
    } = this.state

    return (
      <div>
        <div className={`modal ${modalLogin && 'is-active'}`}>
          <div className="modal-background" onClick={() => this.toggle('modalLogin')}></div>
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title">LOGIN AKUN</p>
              <button
                className="delete"
                aria-label="close"
                onClick={() => this.toggle('modalLogin')}
              />
            </header>
            <form onSubmit={(e) => { this.handleSubmitLogin(e) }} >
              <section className="modal-card-body">
                <div className="field">
                  <div className="control has-icons-left has-icons-right">
                    <input
                      className="input"

                      placeholder="Email"
                      style={{ width: 350 }}
                      onChange={(e) => { this.handleEmailLogin(e) }}
                      value={emailLogin}
                      name="emailLogin"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>
                  </div>
                </div>
                <div className="field">
                  <div className="control has-icons-left">
                    <input
                      className="input"
                      type={password}
                      placeholder="Password"
                      style={{ width: 350 }}
                      value={passwordLogin}
                      onChange={(e) => { this.handlePasswordLogin(e) }}
                      name="passwordLogin"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-lock" />
                    </span>
                    <a onClick={this.handlePassword} className="button" style={{ border: 'none', marginLeft: '10px' }}>
                      <span className="icon is-small is-right">
                        <i className={icon} />
                      </span>
                    </a>
                  </div>
                </div>

              </section>
              <footer className="modal-card-foot">
                <button
                  className="button is-success"
                  onClick={() => this.handleAkun('modalLogin')}
                  type="submit"
                >
                  LOGIN
                </button>
              </footer>
            </form>
          </div>
        </div>



        <div className={`modal ${modalRegis && 'is-active'}`}>
          <div className="modal-background" onClick={() => this.toggle('modalRegis')}></div>
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title">REGISTER</p>
              <button
                className="delete"
                aria-label="close"
                onClick={() => this.toggle('modalRegis')}
              />
            </header>
            <form onSubmit={(e) => { this.handleSubmitRegist(e) }}>
              <section className="modal-card-body">
                <div className="field">
                  <p>NAMA</p>
                  <div className="control ">
                    <input
                      className="input"
                      placeholder="Masukan nama"
                      style={{ width: 350 }}
                      value={namaRegist}
                      name="namaRegist"
                      onChange={(e) => this.handleNamaRegist(e)}
                    />
                    
                    {
                      namaRegist === "" ?
                        <div style={{ color: 'red', fontSize: 12 }}>{namaRegistError}</div>
                        :
                        <div></div>
                    }
                  </div>
                </div>
                <div className="field">
                  <p>EMAIL</p>
                  <div className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="email"
                      placeholder="Email"
                      style={{ width: 350 }}
                      value={emailRegist}
                      onChange={(e) => this.handleEmailRegist(e)}
                      name="emailRegist"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>
                    
                  </div>
                </div>
                <div className="field">
                  <p>PASSWORD</p>
                  <div className="control has-icons-left">
                    <input
                      className="input"
                      type={password}
                      placeholder="Password"
                      style={{ width: 350 }}
                      value={passwordRegist}
                      onChange={(e) => this.handlePasswordRegist(e)}
                      name="passwordRegist"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-lock" />
                    </span>
                    <a onClick={this.handlePassword} className="button" style={{ border: 'none', marginLeft: '10px' }}>
                      <span className="icon is-small is-right">
                        <i className={icon} />
                      </span>
                    </a>
                    {
                      passwordRegist.length === 0 ?
                        <div style={{ color: 'red', fontSize: 12 }}>*password tidak boleh kosong</div>
                        :
                        <div style={{ color: 'red', fontSize: 12 }}>{passwordRegistError}</div>
                    }
                  </div>
                </div>
                <div className="field">
                  <p>Phone</p>
                  <div className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="number"
                      placeholder="Phone"
                      style={{ width: 350 }}
                      value={phoneRegist}
                      onChange={(e) => this.handlePhoneRegist(e)}
                      name="phoneRegist"
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>
                  </div>
                </div>
              </section>
              <footer className="modal-card-foot">
                <button className="button is-success" >REGISTER</button>
              </footer>
            </form>
          </div>
        </div>



        <div className={`modal ${modalEdit && 'is-active'}`}>
          <div className="modal-background" onClick={() => this.toggle('modalEdit')}></div>
          <div className="modal-card">

            <header className="modal-card-head">
              <p className="modal-card-title">EDIT PROFILE</p>
              <button
                className="delete"
                aria-label="close"
                onClick={() => this.toggle('modalEdit')}
              />
            </header>


            <form onSubmit={(e) => { this.handleSubmitEdit(e) }}>
              <section className="modal-card-body" >


                <div className="field">
                  <p>Nama</p>
                  <div className="control ">
                    <input
                      className="input"
                      type="text"
                      name="namaEdit"
                      value={user.userName}
                      placeholder="Masukan nama baru"
                      style={{ width: 350 }}
                      onChange={(e) => { this.handleNamaEdit(e) }}

                    />
                  </div>
                </div>
                <div className="field">
                  <p>Email</p>
                  <div className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="email"
                      name="emailEdit"
                      value={user.userEmail}
                      placeholder="masukan email baru"
                      style={{ width: 350 }}
                      onChange={(e) => { this.handleEmailEdit(e) }}
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-envelope" />
                    </span>
                    <span className="icon is-small is-right">
                      <i className="fa fa-check" />
                    </span>
                  </div>
                </div>
                <div className="field">
                  <p>Password</p>
                  <div className="control has-icons-left">
                    <input
                      className="input"
                      type={password}
                      name="passwordEdit"
                      value={user.userPass}
                      placeholder="masukan password baru"
                      style={{ width: 350 }}
                      onChange={(e) => { this.handlePasswordEdit(e) }}
                    />
                    <span className="icon is-small is-left">
                      <i className="fa fa-lock" />
                    </span>
                    <a onClick={this.handlePassword} className="button" style={{ border: 'none', marginLeft: '10px' }}>
                      <span className="icon is-small is-right">
                        <i className={icon} />
                      </span>
                    </a>
                  </div>
                </div>
                <div className="field">
                  <p>Phone</p>
                  <div className="control ">
                    <input
                      className="input"
                      type="textarea"
                      name="phoneEdit"
                      value={user.userPhone}
                      style={{ width: 350 }}
                      placeholder="masukan nomor hp baru"
                      onChange={(e) => { this.handlePhoneEdit(e) }}
                    />
                  </div>
                </div>
              </section>
              <footer className="modal-card-foot">
                <button className="button is-success" >SAVE</button>
              </footer>
            </form>


          </div>
        </div>


        <nav className="navbar is-fixed-top navHead" role="navigation" aria-label="main navigation" style={{ background: '#209cee' }}>
          <div className="navbar-brand"  >
            <NavLink to="/">
              <div className="navbar-item" >
                <img src={PUBLIC_URL + '/img/logo.jpg'} className="logo is-rounded" />
              </div>
            </NavLink>
            <a role="button" onClick={this.handleNavbar} className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>
          </div>
          <div id="navbarBasicExample" ref="toggle" className={this.state.navbar}>
            <div className="navbar-start">
            </div>
            <div className="navbar-end">
              <div className="navbar-item">
                {
                  login ?
                    <div>
                <span style={{ color: 'white' }}>selamat datang {user.userName}</span>
                      {/*
                      {
                        persons.filter(searchingFor(user)).map(person =>(
                        <div style={{ color:'white',float:"left"}} key={person.userID}>
                          <span >Selamat datang </span>
                          <span>{person.userID}</span>
                        </div>
                      ))}
                      */}
                      <img src={PUBLIC_URL + '/img/avatar.jpg'} className="login" />

                      <div className={dropdown}>
                        <div className="dropdown-trigger">
                          <button onClick={this.dropdownProfile} className="button" aria-haspopup="true" aria-controls="dropdown-menu6">
                            <span >Profile</span>
                            <span className="icon is-small">
                              <i className="fa fa-angle-down" aria-hidden="true"></i>
                            </span>
                          </button>
                        </div>
                        <div className="dropdown-menu" id="dropdown-menu6" role="menu">
                          <div className="dropdown-content">
                            <div className="dropdown-item">
                              <a onClick={(e) => { this.handleEdit(e) }} className="dropdown-item">Edit Profile</a>
                            </div>
                            <div className="dropdown-item">
                              <NavLink to="/StatusBooking"><a className="dropdown-item">My Booking</a></NavLink>
                            </div>
                            <div className="dropdown-item">
                              <a className="dropdown-item">History</a>
                            </div>
                            <div className="dropdown-item">
                              <NavLink to="/"><a onClick={this.logout} className="dropdown-item">Logout</a></NavLink>
                            </div>
                          </div>
                        </div>
                      </div>


                    </div>
                    :
                    <div>

                      <a
                        className="button is-success"
                        onClick={(e) => { this.handleLogin(e) }}
                        style={{ width: 100, height: 50, marginRight: 5 }}>
                        LOGIN
                        </a>
                      <a
                        className="button is-info"
                        style={{ width: 100, height: 50 }}
                        onClick={(e) => { this.handleRegister(e) }}>
                        REGISTER
                       </a>
                    </div>
                }

              </div>
            </div>
          </div>
        </nav>


      </div>

    );
  }
}
const mapStateToProps = (state) => {
  return state
}

export default connect(mapStateToProps, {setUser} /*mapDispachToProps*/)(Navbar);