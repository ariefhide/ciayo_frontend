import React, { Component } from 'react';
import 'bulma/css/bulma.css';
import './App.css';
import Home from './views/Pages/Home';
import SearchResult from './views/Pages/SearchResult';
import BookingSummary from './views/Pages/BookingSummary';
import BookingDetail from './views/Pages/BookingDetail';
import Transaksi from './views/Pages/Transaksi';
import Navbar from './views/Pages/Navbar';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Error from "./views/Pages/Error";
import SideMenu from './views/Pages/SideMenu';
import Date from './views/Pages/Date';
import StatusBooking from './views/Pages/StatusBooking';

import { connect } from 'react-redux';

class App extends Component {


  render() {
    return (
      <BrowserRouter>
      <div>
        <Navbar />
         <div className="columns is-gapless">
          <div className="column is-2">
            <SideMenu  />
          </div>
          <div className="column is-10">
          <Switch>
            <Route path="/" component={Home}  exact />
            <Route path="/SearchResult" component={SearchResult} />
            <Route path="/BookingSummary" component={BookingSummary}/>
            <Route path="/BookingDetail" component={BookingDetail}/>
            <Route path="/Transaksi" component={Transaksi}/>
            <Route path="/Date" component={Date}/>
            <Route path="/StatusBooking" component={StatusBooking}/>
            <Route component={Error} />
          </Switch>
          
          </div>
         </div> 
      </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => {
  return state
}

export default connect(mapStateToProps, /*mapDispachToProps*/)(App);