const url = {
    development: 'http://localhost:5000/users'
  }
  
  const urlPublic = {
    development: 'http://localhost:3000'
  }
  
  const ENV = process.env.REACT_APP_BUILD_ENV || 'development'
  
  export const BASE_URL = url[ENV]
  
  export const PUBLIC_URL = urlPublic[ENV]